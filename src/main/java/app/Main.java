package app;

import java.math.BigInteger;

public class Main {


    public static void main(String[] args) throws AmountNotInRangeException {
        Main app = new Main();
        Amount amount = new Amount(BigInteger.valueOf(999));
        if (!amount.needsApproval()) {
            System.out.println(amount + " does not require approval");
        }
        Amount amount2 = new Amount(BigInteger.valueOf(2000));
        if (!amount2.needsApproval()) {
            System.out.println(amount + " requires approval");
        }
    }

    public boolean approval(int amount) {
        try {
            Amount thisAmount = new Amount(BigInteger.valueOf(amount));
            return thisAmount.needsApproval();
        } catch (AmountNotInRangeException amountNotInRangeEx) {
            return true;
        }
    }

}
