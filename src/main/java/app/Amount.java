package app;


import java.math.BigInteger;
import java.util.Objects;

public class Amount {
    private static final int threshold = 1000;

    private BigInteger amount;

    public Amount(BigInteger amount) throws AmountNotInRangeException {
        if (!isValidAmount(amount)) {
            throw new AmountNotInRangeException("Invalid amount");
        }
        this.amount = amount;
    }

    public static Amount from(BigInteger amount) throws AmountNotInRangeException {
        return new Amount(amount);
    }

    private boolean isValidAmount(BigInteger amount) {
        return amount.longValue() > 0 && amount.longValue() < 100000;
    }

    boolean needsApproval(){
        return amount.longValue() >= threshold;
    }

    public Amount plus(Amount toAddAmount) throws AmountNotInRangeException {
        BigInteger total = amount.add(toAddAmount.amount);
        return Amount.from(total);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Amount amount1 = (Amount) o;
        return Objects.equals(amount, amount1.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount);
    }
}

class AmountNotInRangeException extends Exception {
    AmountNotInRangeException(String message) {
        super(message);
    }
}