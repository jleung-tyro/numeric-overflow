package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {

    @Test
    public void BiggerThanIntMaxSizeNeedsApproval() {
        Main app = new Main();
        boolean res = app.approval(2147483647+1);
        assertTrue(res,() -> "Bigger than int max size needs approval");
    }

    @Test
    public void LessThanIntMinSizeNeedsApproval() {
        Main app = new Main();
        boolean res = app.approval(-2147483648-1);
        assertTrue(res,() -> "Less than int min size needs approval");
    }
}
