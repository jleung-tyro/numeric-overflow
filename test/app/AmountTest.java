package app;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class AmountTest {

    @Test
    void shouldThrowExceptionWhenCreatingAmountHigherThanSupportedRange() {
        assertThrows(AmountNotInRangeException.class, () -> {
            Amount.from(BigInteger.valueOf(2000000));
        });
    }

    @Test
    void shouldThrowExceptionWhenCreatingAmountLowerThanSupportedRange() {
        assertThrows(AmountNotInRangeException.class, () -> {
            Amount.from(BigInteger.valueOf(-1));
        });
    }

    @Test
    void shouldThrowExceptionGivenAddingTwoAmountsTotalHigherThanValidRange() throws AmountNotInRangeException {
        Amount amount1 = Amount.from(BigInteger.valueOf(90000));
        Amount amount2 = Amount.from(BigInteger.valueOf(90000));
        assertThrows(AmountNotInRangeException.class, () -> {
           amount1.plus(amount2);
        });
    }

    @Test
    void shouldAddTwoAmountsTotalInValidRange() throws AmountNotInRangeException {
        Amount amount1 = Amount.from(BigInteger.valueOf(10000));
        Amount amount2 = Amount.from(BigInteger.valueOf(10000));
        assertEquals(Amount.from(BigInteger.valueOf(20000)), amount1.plus(amount2));
    }
}